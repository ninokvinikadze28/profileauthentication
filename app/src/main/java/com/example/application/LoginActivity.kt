package com.example.application

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.Toast
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.ktx.auth
import com.google.firebase.ktx.Firebase
import kotlinx.android.synthetic.main.activity_login.*
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.activity_main.signInButton

class LoginActivity : AppCompatActivity() {
    private lateinit var auth: FirebaseAuth
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)
        init()
    }

    private fun init() {
        auth = Firebase.auth
        signInButton.setOnClickListener {
            signIn()
        }
    }


    private fun signIn() {
        val email = emailEditText.text.toString()
        val password = passwordEditText.text.toString()
            if(email.isNotEmpty() && password.isNotEmpty()){
                signInButton.isClickable = false
                val intent = Intent(this, profileActivity::class.java)
                startActivity(intent)
                auth.signInWithEmailAndPassword(email, password)
                    .addOnCompleteListener(this) { task ->
                        signInButton.isClickable = true
                        if (task.isSuccessful) {
                            // Sign in success, update UI with the signed-in user's information
                            Log.d("signIn", "signInWithEmail:success")
                            Toast.makeText(this, "Authentication is success!", Toast.LENGTH_SHORT).show()
                            val user = auth.currentUser
                        }
                        else {
                            // If sign in fails, display a message to the user.
                            Log.d("signIn", "signInWithEmail:failure", task.exception)
                            Toast.makeText(baseContext, task.exception.toString(), Toast.LENGTH_SHORT).show()

                        }

                    }

            }
         else { Toast.makeText(this, "PLEASE FILL ALL FIELDS", Toast.LENGTH_SHORT).show()
            }

    }
}