package com.example.application

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        init()
    }
private fun init(){
    signInButton.setOnClickListener {
        val intent = Intent(this,LoginActivity::class.java)
        startActivity(intent)

    }
    signUpButton.setOnClickListener {
        val intent = Intent(this,SignupActivity::class.java)
        startActivity(intent)
    }
}

    }

