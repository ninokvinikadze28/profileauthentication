package com.example.application

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.Toast
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.ktx.auth
import com.google.firebase.ktx.Firebase
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.activity_main.signUpButton
import kotlinx.android.synthetic.main.activity_signup.*

class SignupActivity : AppCompatActivity() {
    private lateinit var auth: FirebaseAuth
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_signup)
        init()
    }
    private fun init(){
        auth = Firebase.auth
        signUp.setOnClickListener {
            signUp()
        }}
        private fun signUp(){

            val email = emailEditText.text.toString()
            val password = passwordEditText.text.toString()
            val repeatPassword = repeatPasswordEditText.text.toString()
            if(email.isNotEmpty() && password.isNotEmpty() && repeatPassword.isNotEmpty()){
                if(password == repeatPassword){
                    signUpButton.isClickable = false
                    auth.createUserWithEmailAndPassword(email, password)
                        .addOnCompleteListener(this) { task ->
                            signUpButton.isClickable=true
                            if (task.isSuccessful) {
                                // Sign in success, update UI with the signed-in user's information
                                Log.d("signUp", "createUserWithEmail:success")
                                Toast.makeText(this,"SignUp is success", Toast.LENGTH_SHORT).show()
                                val user = auth.currentUser
                            }
                            else {
                                // If sign in fails, display a message to the user.
                                Log.d("signUp", "createUserWithEmail:failure", task.exception)
                                val show: Any = Toast.makeText(baseContext, task.exception.toString(),
                                    Toast.LENGTH_SHORT).show()

                            }
                        }

                }else{
                    Toast.makeText(this,"Passwords don't match!", Toast.LENGTH_SHORT).show()
                }

            }else{
                Toast.makeText(this,"Please fill all fields!", Toast.LENGTH_SHORT).show()
            }
        }
}