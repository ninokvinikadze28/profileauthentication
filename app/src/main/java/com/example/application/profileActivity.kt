package com.example.application

import android.graphics.BitmapFactory
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.bumptech.glide.Glide
import kotlinx.android.synthetic.main.activity_profile.*


class profileActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_profile)
        init()
    }
    private fun init(){
        Glide.with(this)
            .load("https://i.pinimg.com/564x/ba/c3/20/bac320034b830ae77a0ed15a071a033f.jpg")
            .placeholder(R.drawable.ic_launcher_background)
            .into(coverImageView)
        Glide.with(this)
            .load("https://image.flaticon.com/icons/png/512/17/17603.png")
            .placeholder(R.drawable.ic_launcher_background)
            .into(profileImageView)


    }

}